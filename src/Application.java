import Component.*;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        ChessGame chessGame = new ChessGame();
        boolean gameOver = false;
        String currentPlayer = "p1";
        Scanner scanner = new Scanner(System.in);
        String input;
        ChessLocation chessLocation;
        Component component;
        King king;


            try {
                System.out.println(chessGame.getChessBoard().toString());
                System.out.println(currentPlayer + "'s Turn:");
                input = scanner.nextLine();
                if (input.equalsIgnoreCase("M") || input.equalsIgnoreCase("MOVE")) {

                    if (currentPlayer.equals("p1")) {
                        king = chessGame.getPlayer1King();
                    } else {
                        king = chessGame.getPlayer2King();
                    }
                    Component checkMate = king.check();
                    if (checkMate != null) {
                        System.out.println("Check Mate");
                    }

                    component = getCurrentPiece(chessGame, currentPlayer);
                    chessLocation = getNewLocation();

                    if (component.moveTo(chessLocation)) {
                        currentPlayer = (currentPlayer.equalsIgnoreCase("p1")) ? "p2" : "p1";
                    } else {
                        System.out.println("Move was invalid, try again.");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

    }

    private static Component getCurrentPiece(ChessGame chessGame, String currentPlayer) {
        Scanner scanner = new Scanner(System.in);
        String input;
        ChessLocation currentLocation;
        Component currentPiece;

        while (true) {
            System.out.println("Move from: row, col");
            input = scanner.nextLine();
            currentLocation = createChessLocation(input);
            currentPiece = chessGame.getChessBoard().getPieceAt(currentLocation);
            if (currentPiece == null) {
                System.out.println("Invalid piece selected, out of bounds.");
            } else if (currentPiece.getOwner().equalsIgnoreCase(currentPlayer)) {
                return currentPiece;
            } else {
                System.out.println("Invalid piece selected, not your piece.");
            }
        }
    }

    private static ChessLocation getNewLocation() {
        Scanner scanner = new Scanner(System.in);
        String input;

        ChessLocation newLocation;

        while (true) {
            input = scanner.nextLine();
            newLocation = createChessLocation(input);
            if (!ChessBoard.locationInBounds(newLocation)) {
                System.out.println("Invalid location selected");
            } else {
                return newLocation;
            }
        }
    }

    private static ChessLocation createChessLocation(String input) {
        int row = Integer.parseInt(input.split(",")[0].trim());
        int col = Integer.parseInt(input.split(",")[1].trim());
        return new ChessLocation(row, col);
    }
}
