package Component;


public class Queen extends Component {
    

    public Queen(String owner, ChessLocation initialLocation, ChessGame game) {
        super(owner, initialLocation, game);
        if (owner.equalsIgnoreCase("p1")) {
            id = 'Q';
        } else if (owner.equalsIgnoreCase("p2")) {
            id = 'q';
        }
    }


    @Override
    public boolean moveTo(ChessLocation location) {
        return checkLineOfSight(chessLocation, location) && super.moveTo(location);
    }


    @Override
    protected void updateLoc() {
        threateningLocations.clear();

        super.updateVertical(1);
        super.updateVertical(-1);

        super.updateHorizontal(1);
        super.updateHorizontal(-1);

        super.updateDiagonal(1, 1);
        super.updateDiagonal(-1, 1);
        super.updateDiagonal(1, -1);
        super.updateDiagonal(-1, -1);
    }
}
