package Component;

public class ChessGame {

    private ChessBoard chessBoard;
    private King player1King;
    private King player2King;


    public ChessGame() {
        chessBoard = new ChessBoard();
        setupTeam(0, "p1");
        setupTeam(7, "p2");
    }


    private void setupTeam(int side, String player) {
        int one = (side > 0) ? -1: 1;
        int colIncerment = 0;

        // Elephant
        Component r1 = new Elephant(player, new ChessLocation(side, colIncerment), this);
        Component r2 = new Elephant(player, new ChessLocation(side, 7-colIncerment), this);
        colIncerment += 1;

        // Horse
        Component n1 = new Horse(player, new ChessLocation(side, colIncerment), this);
        Component n2 = new Horse(player, new ChessLocation(side, 7-colIncerment), this);
        colIncerment += 1;

        // camel
        Component b1 = new Camel(player, new ChessLocation(side, colIncerment), this);
        Component b2 = new Camel(player, new ChessLocation(side, 7-colIncerment), this);
        colIncerment += 1;

        // King & Queen
        if (player.equalsIgnoreCase("player1")) {
            player1King = new King(player, new ChessLocation(side, colIncerment), this);
        } else {
            player2King = new King(player, new ChessLocation(side, colIncerment), this);
        }

        Component q = new Queen(player, new ChessLocation(side, 7-colIncerment), this);

        // Soldier
        for (int i = 0; i < 8; i++) {
            Component p = new Soldier(player, new ChessLocation(side + one, i), this);
        }
    }

    /**
     * Returns the Component.ChessBoard.
     * @return The board object of the chess game.
     */
    public ChessBoard getChessBoard() {
        return chessBoard;
    }

    public King getPlayer1King() {
        return player1King;
    }

    public King getPlayer2King() {
        return player2King;
    }
}
