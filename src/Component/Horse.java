package Component;



public class Horse extends Component {


    public Horse(String owner, ChessLocation initialLocation, ChessGame game) {
        super(owner, initialLocation, game);
        if (owner.equalsIgnoreCase("p1")) {
            id = 'N';
        } else if (owner.equalsIgnoreCase("p2")) {
            id = 'n';
        }
    }


    @Override
    public boolean moveTo(ChessLocation location) {
        if (Math.abs(chessLocation.getRow() - location.getRow()) == 2 && 
            Math.abs(chessLocation.getCol() - location.getCol()) == 1) {

            return super.moveTo(location); 
        } else if (Math.abs(chessLocation.getRow() - location.getRow()) == 1 && 
                   Math.abs(chessLocation.getCol() - location.getCol()) == 2) {

            return super.moveTo(location); 
        }
        return false;
    }


    @Override
    protected void updateLoc() {
        int[] rowMoves = { -2, -1, 1, 2, -2, -1, 1, 2 };
        int[] colMoves = { 1, 2, 2, 1, -1, -2, -2, -1 };

        threateningLocations.clear();
        for (int i = 0; i < 8; i++) {
            ChessLocation location = new ChessLocation(rowMoves[i], colMoves[i]);
            if (ChessBoard.locationInBounds(location)) {
                Component piece = chessGame.getChessBoard().getPieceAt(location);

                if (piece != null &&
                    !piece.getOwner().equals(owner)) {

                    threateningLocations.add(location);
                }
            }
        }
    }
}
