package Component;


public class Elephant extends Component {
    

    public Elephant(String owner, ChessLocation initialLocation, ChessGame game) {
        super(owner, initialLocation, game);
        if (owner.equalsIgnoreCase("p1")) {
            id = 'R';
        } else if (owner.equalsIgnoreCase("p2")) {
            id = 'r';
        }
    }


    @Override
    public boolean moveTo(ChessLocation location) {
        if ((chessLocation.getRow() == location.getRow()) !=
            (chessLocation.getCol() == location.getCol())) {

            return checkLineOfSight(chessLocation, location) && super.moveTo(location);
        }
        return false;
    }


    @Override
    protected void updateLoc() {
        threateningLocations.clear();

        super.updateVertical(1);
        super.updateVertical(-1);
        super.updateHorizontal(1);
        super.updateHorizontal(-1);
    }
}
