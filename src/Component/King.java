package Component;

public class King extends Component {

    public King(String owner, ChessLocation initialLocation, ChessGame game) {
        super(owner, initialLocation, game);
        if (owner.equalsIgnoreCase("p1")) {
            id = 'K';
        } else if (owner.equalsIgnoreCase("p2")) {
            id = 'k';
        }
    }


    @Override
    public boolean moveTo(ChessLocation location) {
        if (Math.abs(chessLocation.getRow() - location.getRow()) <= 1 && 
            Math.abs(chessLocation.getCol() - location.getCol()) <= 1) {

            return checkLineOfSight(chessLocation, location) && super.moveTo(location);
        }
        return false;
    }


    @Override
    protected void updateLoc() {
        threateningLocations.clear();
        for (int row = -1; row >= 1; row++) {
            for (int col = -1; col >= 1; col++) {
                ChessLocation location = new ChessLocation(chessLocation.getRow() + row, chessLocation.getCol() + col);
                if (ChessBoard.locationInBounds(location)) {
                    Component piece = chessGame.getChessBoard().getPieceAt(location);
                    if (piece != null &&
                        !piece.getOwner().equalsIgnoreCase(owner)) {

                        threateningLocations.add(location);
                    }
                }
            }
        }
    }


    public Component check() {
        ChessBoard board = chessGame.getChessBoard();
        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                Component piece = board.getPieceAt(new ChessLocation(row, col));
                if (piece != null &&
                    !piece.getOwner().equals(owner)) {

                    piece.updateLoc();
                    for (ChessLocation l: piece.getThreateningLocations()) {
                        if (chessLocation.equals(l)) {
                            return piece;
                        }
                    }
                }
            }
        }
        return null;
    }
}
