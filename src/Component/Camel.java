package Component;


public class Camel extends Component {

    public Camel(String owner, ChessLocation initialLocation, ChessGame game) {
        super(owner, initialLocation, game);
        if (owner.equalsIgnoreCase("p1")) {
            id = 'B';
        } else if (owner.equalsIgnoreCase("p2")) {
            id = 'b';
        }
    }


    @Override
    public boolean moveTo(ChessLocation location) {
        if (Math.abs(chessLocation.getRow() - location.getRow()) == 
            Math.abs(chessLocation.getCol() - location.getCol())) {
            
            return checkLineOfSight(chessLocation, location) && super.moveTo(location);
        }
        return false;
    }

    @Override
    protected void updateLoc() {

    }
}
