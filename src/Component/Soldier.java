package Component;

public class Soldier extends Component {

    private boolean firstMove;
    private int one;


    public Soldier(String owner, ChessLocation initialLocation, ChessGame game) {
        super(owner, initialLocation, game);
        if (owner.equalsIgnoreCase("p1")) {
            id = 'P';
            one = 1;
        } else if (owner.equalsIgnoreCase("p2")) {
            id = 'p';
            one = -1;
        }
        firstMove = true;
    }

    @Override
    public boolean moveTo(ChessLocation location) {
        if (location.getCol() == chessLocation.getCol()) {
            if (location.getRow() - chessLocation.getRow() == one) {
                if (firstMove) {
                    firstMove = false;
                }
                return !chessGame.getChessBoard().isPieceAt(location.getRow(), location.getCol()) && super.moveTo(location);
            } else if (firstMove && (location.getRow() - chessLocation.getRow() == (one * 2))) {
                if (firstMove) {
                    firstMove = false;
                }
                return !chessGame.getChessBoard().isPieceAt(location.getRow(), location.getCol()) && super.moveTo(location);
            }
        } else if (Math.abs(location.getCol() - chessLocation.getCol()) == 1) {
            if (chessGame.getChessBoard().isPieceAt(location.getRow(), location.getCol()) &&
                location.getRow() - chessLocation.getRow() == one) {

                if (firstMove) {
                    firstMove = false;
                }
                return super.moveTo(location);
            }
        }
        return false;
    }

    /**
     * Updates the threatening locations.
     */
    @Override
    protected void updateLoc() {
        int one = 0;
        if (owner.equalsIgnoreCase("p1") &&
            chessLocation.getRow() <= 6) {
            one = 1;
        } else if (owner.equalsIgnoreCase("p2") &&
                    chessLocation.getRow() >= 1) {
            one = -1;
        }

        threateningLocations.clear();

        if (chessLocation.getCol() >= 1) {
            threateningLocations.add(new ChessLocation(chessLocation.getRow() + one, chessLocation.getCol() - 1));
        }
        if (chessLocation.getCol() <= 6) {
            threateningLocations.add(new ChessLocation(chessLocation.getRow() + one, chessLocation.getCol() + 1));
        }
    }
}
